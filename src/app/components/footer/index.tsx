import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faFileAlt, faUser } from "@fortawesome/free-regular-svg-icons";
import { faCarAlt, faCog, faHome, faUserCog } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import styled from "styled-components";
import tw from "twin.macro";
import { Logo } from "../logo";
import { SCREENS } from "../responsive";

const FooterContainer = styled.div`
    ${tw`
        flex
        flex-col
        min-w-full
        bg-red-500
        pt-10
        md:pt-16
        items-center
        justify-center
    `};
`;

const InnerContainer = styled.div`
    ${tw`
        flex
        w-full
        h-full
        max-w-screen-2xl
        flex-wrap
    `};
`;

const BottomContainer = styled.div`
    ${tw`
        bg-red-600
        w-full
        flex
        justify-center
        mt-10
        pt-2
        pb-2
        md:mt-10
    `};
`;

const CopyrightText = styled.small`
    font-size: 12px;
    ${tw`
        text-gray-300
    `};
`;

const AboutContainer = styled.div`
    ${tw`
        flex
        flex-col
        mr-2
        md:mr-16
        pl-10
        pr-10
        md:pl-3
        md:pr-3
    `};
`;

const AboutText = styled.div`
    ${tw`
        text-white
        text-sm
        font-normal
        max-w-xs
        leading-5
        mt-2
    `};
`;

const SectionContainer = styled.div`
    ${tw`
        w-full
        md:w-auto
        flex
        flex-col
        mr-2
        md:mr-16
        pl-10
        pr-10
        md:pl-3
        md:pr-3
        mt-7
        md:mt-0
    `};
`;

const LinksContainer = styled.ul`
    ${tw`
        outline-none
        list-none
        flex
        flex-col
    `};
`;

const ListItem = styled.li`
    ${tw`
        mb-3
    `};

    & > a {
        ${tw`
            text-sm
            text-white
            transition-all
            hover:text-gray-600
        `}
    }
`;

const HeaderTitle = styled.h3`
    ${tw`
        text-xl
        font-bold
        text-white
        mb-3
    `};
`;

const HorizontalContainer = styled.div`
    ${tw`
        flex
        items-center
    `};
`;

const GreenIcon = styled.span`
    border-radius: 50%;
    ${tw`
        w-5
        h-5
        rounded-full
        flex
        items-center
        justify-center
        text-white
        text-base
        mr-3
        bg-green-600
        p-4
        border-transparent
    `};
`;

const SmallText = styled.h6`
    ${tw`
        text-sm
        text-white
    `};
`;

export function Footer() {
    return(
        <FooterContainer>
            <InnerContainer>
                <AboutContainer>
                    <Logo color="white"/>
                    <AboutText>
                        Carrent is Rent Car Company located in Jakarta with various Cars and various Price.
                        Our cars is on top condition, we always check before you rent the cars.
                    </AboutText>
                </AboutContainer>
                <SectionContainer>
                    <HeaderTitle>Our Links</HeaderTitle>
                    <LinksContainer>
                        <ListItem><a href="#">Home</a></ListItem>
                        <ListItem><a href="#">About Us</a></ListItem>
                        <ListItem><a href="#">Services</a></ListItem>
                        <ListItem><a href="#">Models</a></ListItem>
                        <ListItem><a href="#">Blog</a></ListItem>
                    </LinksContainer>
                </SectionContainer>
                <SectionContainer>
                    <HeaderTitle>Other Links</HeaderTitle>
                    <LinksContainer>
                        <ListItem><a href="#">FAQ</a></ListItem>
                        <ListItem><a href="#">Contact Us</a></ListItem>
                        <ListItem><a href="#">Support</a></ListItem>
                        <ListItem><a href="#">Privacy Policy</a></ListItem>
                        <ListItem><a href="#">Terms & Condition</a></ListItem>
                    </LinksContainer>
                </SectionContainer>
                <SectionContainer>
                    <HeaderTitle>Call Now</HeaderTitle>
                    <HorizontalContainer>
                        <GreenIcon>
                            <FontAwesomeIcon icon={faWhatsapp} />
                        </GreenIcon>
                        <SmallText>082221114716</SmallText>
                    </HorizontalContainer>
                </SectionContainer>
                <SectionContainer>
                    <HeaderTitle>Email</HeaderTitle>
                    <HorizontalContainer>
                        <GreenIcon>
                            <FontAwesomeIcon icon={faEnvelope} />
                        </GreenIcon>
                        <SmallText>email@carrent.id</SmallText>
                    </HorizontalContainer>
                </SectionContainer>
            </InnerContainer>
            <BottomContainer>
                <CopyrightText>Copyright &copy; {new Date().getFullYear()} Carrent. All rights reserved</CopyrightText>
            </BottomContainer>
        </FooterContainer>
    )

}