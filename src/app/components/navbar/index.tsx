import React from "react";
import styled from "styled-components";
import tw from "twin.macro";
import { Logo } from "../logo";
import { SCREENS } from "../responsive";
import { NavItems } from "./navItems";

const NavbarContainer = styled.div`
    min-height: 68px;
    ${tw`
        w-full
        max-w-screen-2xl
        flex
        flex-row
        items-center
        pl-3
        lg:pl-12
        lg:pr-12
        md:pl-3
        justify-between
    `}
`;

const LogoContainer = styled.div`

`;

export function Navbar() {
    return (
    <NavbarContainer>
        <LogoContainer>
            <Logo />
        </LogoContainer>
        <NavItems />
    </NavbarContainer>
    )
}
